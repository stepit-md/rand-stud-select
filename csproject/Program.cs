using System;
using System.IO;
using System.Collections.Generic;
using rftools;



namespace randselect
{
    class Program
    {
        


        static List<string> pendingNamesLs;
        static List<string> passedNamesLs;
        static Random rand = new Random();

        static private string ArgsString
        {
            get
            {
                string result = "";
                var all = Environment.GetCommandLineArgs();
                for (int i = 1; i < all.Length; ++i)
                    result += $", {all[i]}";
                return result;
            }
        }





        static void Main(string[] args)
        {
            Initialize();
            if (OptionProcessor.Count == 1)
                SelectRoundRobin();
            if (OptionProcessor.ContainsAny(args) == false)
                NoArgumentFits();
            if (OptionProcessor.Contains("-c"))
                ClearPassedList();
            if (OptionProcessor.Contains("-r"))
                SelectRandom();
            if (OptionProcessor.Contains("-p"))
                PrintPassedStuds();
            if (OptionProcessor.Contains("-n"))
                PrintStudNames();
            if (OptionProcessor.Contains("-u"))
                UnPassLatestStud();
            if (OptionProcessor.Contains("-P"))
                PrintNotPassedStuds();
            if (OptionProcessor.Contains("-h"))
                PrintHelp();

        }



        static void Initialize()
        {
            if (File.Exists(KParams.NAMES_FILE_NAME))
            {
                pendingNamesLs = new List<string>(File.ReadAllLines(KParams.NAMES_FILE_NAME));
                // remove empty
                pendingNamesLs.RemoveAll((el) => string.IsNullOrEmpty(el));
            }
            else
                ShowMessageAndHelpAndExit("names file not found at path: " + KParams.NAMES_FILE_NAME);

            if (File.Exists(KParams.PASSED_FILE_NAME))
            {
                passedNamesLs = new List<string>(File.ReadAllLines(KParams.PASSED_FILE_NAME));
                passedNamesLs.RemoveAll((el) => string.IsNullOrEmpty(el));
            }
            else
            {
                File.Create(KParams.PASSED_FILE_NAME);
                passedNamesLs = new List<string>();
            }
        }



        static private void ShowMessageAndExit(string msg)
        {
            Console.WriteLine(msg);
            Environment.Exit(0);
        }

        static private void ShowMessageAndHelpAndExit(string msg)
        {
            Console.WriteLine(msg);
            Console.WriteLine();
            Console.WriteLine(KParams.helpInfo);
            Environment.Exit(0);
        }

        static private void SerializePassedList()
        {
            File.WriteAllLines(KParams.PASSED_FILE_NAME, passedNamesLs.ToArray());
        }

        static private string GetRandomFrom(string[] arr)
        {
            int id = rand.Next(arr.Length);
            return arr[id];
        }

        static private string[] GetNonPassedStuds()
        {
            var ls = new List<string>(pendingNamesLs);
            foreach (var passedStud in passedNamesLs)
            {
                int i = ls.IndexOf(passedStud);
                if (i != -1)
                    ls.RemoveAt(i);
            }
            return ls.ToArray();
        }


        //.................................................................................SELECTORS

        static private void SelectRoundRobin()
        {
            string[] nonPassArr = GetNonPassedStuds();
            if (nonPassArr.Length == 0)
                ShowMessageAndExit("all stud passed. Clear the list!");

            string name = GetRandomFrom(nonPassArr);
            passedNamesLs.Add(name);
            SerializePassedList();

            ShowMessageAndExit(name);
        }



        static private void ClearPassedList()
        {
            passedNamesLs.Clear();
            SerializePassedList();
            ShowMessageAndExit("overpassed list cleared");
        }




        static private void NoArgumentFits()
        {
            ShowMessageAndExit($"can't parse args '{ArgsString}'\n\n{HELP_MSG}");
        }


        static private void SelectRandom()
        {
            string name = GetRandomFrom(pendingNamesLs.ToArray());
            ShowMessageAndExit(name);
        }

        static private void PrintPassedStuds()
        {
            if (passedNamesLs.Count == 0)
            {
                Console.WriteLine("no one overpassed yet");
            }
            else if (passedNamesLs.Count == pendingNamesLs.Count)
            {
                Console.WriteLine("all overpassed");
            }
            else
            {
                Console.WriteLine("printing overpass list, count: " + passedNamesLs.Count);
                foreach (var stud in passedNamesLs)
                    Console.WriteLine(stud);
            }
        }

        static private void PrintStudNames()
        {
            Console.WriteLine("printing all, count: " + pendingNamesLs.Count);
            Console.WriteLine();
            foreach (var s in pendingNamesLs)
                Console.WriteLine(s);
                
        }

        static private void PrintNotPassedStuds()
        {
            string[] pendArr = GetNonPassedStuds();
            Console.WriteLine("printing pending list, count: " + pendArr.Length);
            foreach (var name in pendArr)
                Console.WriteLine(name);
        }

        static private void PrintHelp()
        {
            ShowMessageAndExit(KParams.helpInfo);
        }

        static private void UnPassLatestStud()
        {
            int count = passedNamesLs.Count;
            if (count == 0)
                ShowMessageAndExit("no overpassed persons");
             
            string name = passedNamesLs[count - 1];
            passedNamesLs.RemoveAt(count - 1);
            SerializePassedList();
            ShowMessageAndExit("overpass reverted for: " + name);
        }






        const string HELP_MSG = "";
    }
}
