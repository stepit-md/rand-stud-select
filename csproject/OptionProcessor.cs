using System;


namespace rftools
{
    public static class OptionProcessor
    {
        static string[] _args;

        static OptionProcessor()
        {
             _args = Environment.GetCommandLineArgs();
        }



        public static int Count => _args.Length;

        public static bool Contains(string s)
        {
            if (string.IsNullOrEmpty(s)) throw new ArgumentException("s is null or epty");
            return Array.FindIndex(_args, (el) => el == s) != -1;
        }

        public static bool ContainsAny(string[] options)
        {
            foreach (var opt in options)
                for (int i = 0; i < _args.Length; ++i)
                    if (opt == _args[i]) 
                        return true;

            return false;
        }

    }
}