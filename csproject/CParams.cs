


namespace randselect
{
    public static class KParams
    {
        public const string NAMES_FILE_NAME = "items.ls";
        public const string PASSED_FILE_NAME = "pass.ls";

        public static readonly string helpInfo = string.Format(

@"simple tool to select items round-robin fashion
files of interest '{0}', '{1}'
    usage: ritem [options]
    [no-option] select round-robin fashion and add to pending list {1}
    -h  print this help
    -c  clear 'pass.ls' file
    -r  get random and don't add to overpass list
    -p  print overpass list
    -P  print not passed list (pending items)
    -n  print items names
    -u  undo latest overpassed item from '{1}'
    ", NAMES_FILE_NAME, PASSED_FILE_NAME);

    }
}